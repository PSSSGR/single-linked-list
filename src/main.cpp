/*
 * Filename:	main.cpp
 * Name:		Sri Padala
 * Description:	singly-linked list class template.
  */
#include <iostream>
#include <string>
#include "Money.hpp"
#include "SLList.hpp"
#include "test_interface.hpp"

int main()
{
    runTests();

    return 0;
}
