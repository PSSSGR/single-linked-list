#/*
 #* Filename:	Makefile
 #* Name:		Sri Padala
 #* Description: Singly-linked list class template.
#/
CXX=g++
CXXFLAGS=-Wall -std=c++11
EXE=hw07

SRCDIR = src
reldir = rel
dbgdir = dbg
SRCS = main.cpp Money.cpp

OBJS=$(SRCS:%.cpp=%.o)
relOBJS:=$(addprefix $(reldir)/,$(OBJS))
debugOBJS:=$(addprefix $(dbgdir)/,$(OBJS))

release: prep $(reldir)/$(EXE)#creating exe files in the directory.
debug: prep $(dbgdir)/$(EXE)#creating exe files in the directory.

.PHONY : prep release debug clean

prep:
	@mkdir dbg rel > /dev/null 2>& 1; true

$(reldir)/$(EXE): $(relOBJS)#release
	$(CXX) $(CXXFLAGS) -o $@ $^

$(reldir)/%.o:$(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(dbgdir)/$(EXE): $(debugOBJS)#release
	$(CXX) $(CXXFLAGS) -o $@ $^

$(dbgdir)/%.o:$(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -g -O0 -c -o $@ $<

clean :
	rm	-f $(OBJS) $(relOBJS) $(debugOBJS)  core $(dbgdir)/$(EXE) $(reldir)/$(EXE)
